# 国密SM4加密工具

## 简介

本开源项目提供了一个基于国密算法SM4的加密工具，支持对数据进行加密，适用于中英文及特殊字符的加密处理。该工具不仅支持字符串加密，还可以对文件进行加密操作。此外，工具支持CBC和ECB两种加密模式，满足不同场景下的加密需求。

## 功能特点

- **支持中英文及特殊字符加密**：无论是纯文本还是包含特殊字符的数据，都能进行有效的加密处理。
- **文件加密**：除了字符串加密外，还支持对文件进行加密，保护文件内容的安全性。
- **CBC和ECB模式**：提供CBC和ECB两种加密模式，用户可以根据实际需求选择合适的模式进行加密。

## 使用说明

### 安装与配置

1. **克隆仓库**：
   ```bash
   git clone https://github.com/your-repo/sm4-encryption.git
   ```

2. **安装依赖**：
   ```bash
   cd sm4-encryption
   npm install
   ```

### 加密字符串

```javascript
const sm4 = require('./sm4');

const key = '1234567890abcdef'; // 16字节密钥
const data = 'Hello, 世界!';

// 使用ECB模式加密
const encryptedData = sm4.encrypt(data, key, 'ECB');
console.log('Encrypted Data:', encryptedData);

// 使用CBC模式加密
const iv = 'abcdef1234567890'; // 16字节初始向量
const encryptedDataCBC = sm4.encrypt(data, key, 'CBC', iv);
console.log('Encrypted Data (CBC):', encryptedDataCBC);
```

### 加密文件

```javascript
const fs = require('fs');
const sm4 = require('./sm4');

const key = '1234567890abcdef'; // 16字节密钥
const filePath = './example.txt';

// 读取文件内容
const fileData = fs.readFileSync(filePath, 'utf-8');

// 使用ECB模式加密文件内容
const encryptedFileData = sm4.encrypt(fileData, key, 'ECB');
fs.writeFileSync('./encrypted_example.txt', encryptedFileData);

console.log('File encrypted successfully.');
```

## 贡献

欢迎大家贡献代码，提出问题或建议。请通过提交Issue或Pull Request来参与项目的开发。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。